/*
 * MODEL_sensors.c
 *
 *  Created on: 2017年3月8日
 *      Author: xin
 */

#include "MODEL_sensors.h"

struct RobotFootPressureForceSensorModel robotFootPressureForceSensorModel;

/********************************************
 *
 * 	函数说明：传感器模型初始化
 *
 ********************************************/
void MODEL_sensors_init() {

	// 1. hardware init
	ForceSensorInterface_hardwareInit();

	// 2. data model init
	RobotFootPressureForceSensorModel_init(&robotFootPressureForceSensorModel);
}

/********************************************
 *
 * 	函数说明：更新机器人传感器的值
 *
 ********************************************/
void MODEL_sensors_valueUpdate() {

	RobotFootPressureForceSensorModel_valueUpdate(&robotFootPressureForceSensorModel);
}
