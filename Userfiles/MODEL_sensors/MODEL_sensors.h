/*
 * MODEL_sensors.h
 *
 *  Created on: 2017年3月8日
 *      Author: xin
 */

#ifndef USERFILES_MODEL_SENSORS_MODEL_SENSORS_H_
#define USERFILES_MODEL_SENSORS_MODEL_SENSORS_H_

#include "forceSensor/ForceSensorInterface.h"
#include "forceSensor/ForceSensorModel.h"
#include "forceSensor/FootPressureForceSensorModel.h"
#include "forceSensor/RobotFootPressureForceSensorModel.h"

void MODEL_sensors_init();
void MODEL_sensors_valueUpdate();

#endif /* USERFILES_MODEL_SENSORS_MODEL_SENSORS_H_ */
