/*
 * ForceSensorInterface.c
 *
 *  Created on: 2016-5-23
 *      Author: 康复组
 */
#include "DSP2833x_Device.h"     // DSP2833x Headerfile Include File
#include "ForceSensorInterface.h"

/********************************************
 *
 * 	函数说明：传感器接口数据模型初始化
 *
 ********************************************/
void ForceSensorInterface_init(
		struct ForceSensorInterface * forceSensorInterface,
		int inputMuxIndex,
		int outputMuxIndex,
		int initValue) {

	forceSensorInterface->inputMuxIndex 	= inputMuxIndex;
	forceSensorInterface->outputMuxIndex 	= outputMuxIndex;
	forceSensorInterface->value 			= initValue;
}

/********************************************
 *
 * 	函数说明：传感器接口数据模型参数拷贝
 *
 ********************************************/
void ForceSensorInterface_copy(
		struct ForceSensorInterface * forceSensorInterfaceTo,
		struct ForceSensorInterface * forceSensorInterfaceFrom) {

	forceSensorInterfaceTo->inputMuxIndex 	= forceSensorInterfaceFrom->inputMuxIndex;
	forceSensorInterfaceTo->outputMuxIndex 	= forceSensorInterfaceFrom->outputMuxIndex;
	forceSensorInterfaceTo->value 			= forceSensorInterfaceFrom->value;
}

/********************************************
 *
 * 	函数说明：传感器接口数据模型读取数据(更新数据)
 *
 ********************************************/
void ForceSensorInterface_valueUpdate(
		struct ForceSensorInterface * forceSensorInterface) {

	// 1. input mux selection
	switch (forceSensorInterface->inputMuxIndex) {
	case 0:
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL2_CLEAR;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL1_CLEAR;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL0_CLEAR;
		break;
	case 1:
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL2_CLEAR;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL1_CLEAR;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL0_SET;
		break;
	case 2:
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL2_CLEAR;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL1_SET;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL0_CLEAR;
		break;
	case 3:
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL2_CLEAR;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL1_SET;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL0_SET;
		break;
	case 4:
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL2_SET;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL1_CLEAR;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL0_CLEAR;
		break;
	case 5:
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL2_SET;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL1_CLEAR;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL0_SET;
		break;
	default:
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL2_CLEAR;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL1_CLEAR;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL0_CLEAR;
		break;
	}

	// 2. output mux selection
	switch (forceSensorInterface->outputMuxIndex) {
	case 0:
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL2_CLEAR;
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL1_CLEAR;
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL0_CLEAR;
		break;
	case 1:
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL2_CLEAR;
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL1_CLEAR;
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL0_SET;
		break;
	case 2:
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL2_CLEAR;
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL1_SET;
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL0_CLEAR;
		break;
	case 3:
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL2_CLEAR;
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL1_SET;
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL0_SET;
		break;
	default:
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL2_CLEAR;
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL1_CLEAR;
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL0_CLEAR;
		break;
	}

	// for voltage vibration reduction
	int wait;
	for (wait = 0; wait < 200; ++wait) {}

	// 3. read ADC value
	AdcRegs.ADCTRL2.bit.SOC_SEQ1 = 1;			// Start Convert of SEQ1
	while (AdcRegs.ADCST.bit.INT_SEQ1 == 0); 	// Wait for interrupt
	AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1;       	// Clear INT SEQ1 bit
	AdcRegs.ADCTRL2.bit.RST_SEQ1 = 1;			// Reset SEQ1

	forceSensorInterface->value = AdcRegs.ADCRESULT0 >> 4;
}

/********************************************
 *
 * 	函数说明：传感器接口数据模型硬件初始化
 *
 ********************************************/
void ForceSensorInterface_hardwareInit() {

	EALLOW;

	GpioCtrlRegs.GPCDIR.bit.GPIO80	= 1;
	GpioCtrlRegs.GPCDIR.bit.GPIO81	= 1;
	GpioCtrlRegs.GPCDIR.bit.GPIO82	= 1;
	GpioCtrlRegs.GPCDIR.bit.GPIO83	= 1;
	GpioCtrlRegs.GPCDIR.bit.GPIO84	= 1;
	GpioCtrlRegs.GPCDIR.bit.GPIO85	= 1;
	GpioCtrlRegs.GPCDIR.bit.GPIO86	= 1;
	GpioCtrlRegs.GPCDIR.bit.GPIO87	= 1;

	EDIS;
}



