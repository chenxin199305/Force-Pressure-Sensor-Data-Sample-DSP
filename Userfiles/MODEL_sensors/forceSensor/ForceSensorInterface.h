/*
 * ForceSensorInterface.h
 *
 *  Created on: 2016-5-23
 *      Author: 康复组
 */

#ifndef FORCESENSORINTERFACE_H_
#define FORCESENSORINTERFACE_H_

#define FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL0_SET		(GpioDataRegs.GPCSET.bit.GPIO80 = 1)
#define FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL1_SET		(GpioDataRegs.GPCSET.bit.GPIO81 = 1)
#define FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL2_SET		(GpioDataRegs.GPCSET.bit.GPIO82 = 1)

#define FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL0_SET		(GpioDataRegs.GPCSET.bit.GPIO84 = 1)
#define FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL1_SET		(GpioDataRegs.GPCSET.bit.GPIO85 = 1)
#define FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL2_SET		(GpioDataRegs.GPCSET.bit.GPIO86 = 1)

#define FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL0_CLEAR		(GpioDataRegs.GPCCLEAR.bit.GPIO80 = 1)
#define FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL1_CLEAR		(GpioDataRegs.GPCCLEAR.bit.GPIO81 = 1)
#define FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL2_CLEAR		(GpioDataRegs.GPCCLEAR.bit.GPIO82 = 1)

#define FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL0_CLEAR	(GpioDataRegs.GPCCLEAR.bit.GPIO84 = 1)
#define FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL1_CLEAR	(GpioDataRegs.GPCCLEAR.bit.GPIO85 = 1)
#define FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL2_CLEAR	(GpioDataRegs.GPCCLEAR.bit.GPIO86 = 1)

struct ForceSensorInterface {
	unsigned int inputMuxIndex;
	unsigned int outputMuxIndex;
	unsigned int value;
};

void ForceSensorInterface_init(
		struct ForceSensorInterface * sensorInterfaceModel,
		int inputMuxIndex,
		int outputMuxIndex,
		int initValue);

void ForceSensorInterface_copy(
		struct ForceSensorInterface * sensorInterfaceModelTo,
		struct ForceSensorInterface * sensorInterfaceModelFrom);

void ForceSensorInterface_valueUpdate(
		struct ForceSensorInterface * sensorInterfaceModel);

void ForceSensorInterface_hardwareInit();

#endif /* FORCESENSORINTERFACE_H_ */
