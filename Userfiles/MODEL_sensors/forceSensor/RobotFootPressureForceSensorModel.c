/*
 * RobotFootPressureForceSensorModel.c
 *
 *  Created on: 2017年4月22日
 *      Author: xin
 */

#include "RobotFootPressureForceSensorModel.h"

/********************************************
 *
 * 	函数说明：机器人足底压力传感器数据模型初始化
 *
 ********************************************/
void RobotFootPressureForceSensorModel_init(
		struct RobotFootPressureForceSensorModel * robotFootPressureForceSensorModel) {

	FootPressureForceSensorModel_init(&robotFootPressureForceSensorModel->leftLegFootPressureForceSensorModel);
	FootPressureForceSensorModel_init(&robotFootPressureForceSensorModel->rightLegFootPressureForceSensorModel);
}

/********************************************
 *
 * 	函数说明：机器人足底压力传感器数据模型更新
 *
 ********************************************/
void RobotFootPressureForceSensorModel_valueUpdate(
		struct RobotFootPressureForceSensorModel * robotFootPressureForceSensorModel) {

	FootPressureForceSensorModel_valueUpdate(&robotFootPressureForceSensorModel->leftLegFootPressureForceSensorModel);
//	FootPressureForceSensorModel_valueUpdate(&robotFootPressureForceSensorModel->rightLegFootPressureForceSensorModel);
}


