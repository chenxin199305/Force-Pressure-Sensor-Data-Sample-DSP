/*
 * FootPressureForceSensorModel.h
 *
 *  Created on: 2017年10月5日
 *      Author: xin
 */

#ifndef USERFILES_MODEL_SENSORS_FORCESENSOR_FOOTPRESSUREFORCESENSORMODEL_H_
#define USERFILES_MODEL_SENSORS_FORCESENSOR_FOOTPRESSUREFORCESENSORMODEL_H_

#include "ForceSensorModel.h"

struct FootPressureForceSensorModel {
	struct ForceSensorModel forceSensorModel_1_1;
	struct ForceSensorModel forceSensorModel_1_2;
	struct ForceSensorModel forceSensorModel_1_3;
	struct ForceSensorModel forceSensorModel_1_4;

	struct ForceSensorModel forceSensorModel_2_2;
	struct ForceSensorModel forceSensorModel_2_3;
	struct ForceSensorModel forceSensorModel_2_4;

	struct ForceSensorModel forceSensorModel_3_2;
	struct ForceSensorModel forceSensorModel_3_3;

	struct ForceSensorModel forceSensorModel_4_2;
	struct ForceSensorModel forceSensorModel_4_3;

	struct ForceSensorModel forceSensorModel_5_4;

	struct ForceSensorModel forceSensorModel_6_1;
	struct ForceSensorModel forceSensorModel_6_2;
	struct ForceSensorModel forceSensorModel_6_3;
	struct ForceSensorModel forceSensorModel_6_4;
};

void FootPressureForceSensorModel_init(
		struct FootPressureForceSensorModel * footPressureForceSensorModel);

void FootPressureForceSensorModel_valueUpdate(
		struct FootPressureForceSensorModel * footPressureForceSensorModel);

#endif /* USERFILES_MODEL_SENSORS_FORCESENSOR_FOOTPRESSUREFORCESENSORMODEL_H_ */
