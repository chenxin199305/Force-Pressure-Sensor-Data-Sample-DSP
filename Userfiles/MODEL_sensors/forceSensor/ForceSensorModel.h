/*
 * forceSensorModel.h
 *
 *  Created on: 2017年3月8日
 *      Author: xin
 */

#ifndef USERFILES_MODEL_FORCESENSOR_FORCESENSORMODEL_H_
#define USERFILES_MODEL_FORCESENSOR_FORCESENSORMODEL_H_

#include "ForceSensorInterface.h"

struct ForceSensorModel {
	unsigned int rowIndex;
	unsigned int columnIndex;
	float value;

	struct ForceSensorInterface forceSensorInterface;
};

void ForceSensorModel_init(
		struct ForceSensorModel * forceSensorModel,
		int rowIndex,
		int columnIndex);

void ForceSensorModel_copy(
		struct ForceSensorModel * forceSensorModelTo,
		struct ForceSensorModel * forceSensorModelFrom);

void ForceSensorModel_valueUpdate(
		struct ForceSensorModel * forceSensorModel);


#endif /* USERFILES_MODEL_FORCESENSOR_FORCESENSORMODEL_H_ */
