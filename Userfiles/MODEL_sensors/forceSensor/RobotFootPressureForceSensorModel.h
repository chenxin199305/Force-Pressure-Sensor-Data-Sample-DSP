/*
 * RobotFootForceSensorModel.h
 *
 *  Created on: 2017年4月22日
 *      Author: xin
 */

#ifndef USERFILES_MODEL_SENSORS_FORCESENSOR_ROBOTFOOTPRESSUREFORCESENSORMODEL_H_
#define USERFILES_MODEL_SENSORS_FORCESENSOR_ROBOTFOOTPRESSUREFORCESENSORMODEL_H_

#include "FootPressureForceSensorModel.h"

struct RobotFootPressureForceSensorModel {
	struct FootPressureForceSensorModel leftLegFootPressureForceSensorModel;
	struct FootPressureForceSensorModel rightLegFootPressureForceSensorModel;
};

void RobotFootPressureForceSensorModel_init(
		struct RobotFootPressureForceSensorModel * robotFootPressureForceSensorModel);

void RobotFootPressureForceSensorModel_valueUpdate(
		struct RobotFootPressureForceSensorModel * robotFootPressureForceSensorModel);


#endif /* USERFILES_MODEL_SENSORS_FORCESENSOR_ROBOTFOOTPRESSUREFORCESENSORMODEL_H_ */
