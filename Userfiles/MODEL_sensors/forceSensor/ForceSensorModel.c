/*
 * forceSensorModel.c
 *
 *  Created on: 2017年3月8日
 *      Author: xin
 */

#include "ForceSensorModel.h"

/********************************************
 *
 * 	函数说明：力传感器数据模型初始化
 *
 ********************************************/
void ForceSensorModel_init(
		struct ForceSensorModel * forceSensorModel,
		int rowIndex,
		int columnIndex) {

	forceSensorModel->rowIndex 		= rowIndex;
	forceSensorModel->columnIndex 	= columnIndex;

	// init force sensor interface
	ForceSensorInterface_init(
			&forceSensorModel->forceSensorInterface,
			rowIndex - 1,
			columnIndex - 1,
			0);
}

/********************************************
 *
 * 	函数说明：力传感器数据模型参数拷贝
 *
 ********************************************/
void ForceSensorModel_copy(
		struct ForceSensorModel * forceSensorModelTo,
		struct ForceSensorModel * forceSensorModelFrom) {
}

/********************************************
 *
 * 	函数说明：力传感器数据模型值更新
 *
 ********************************************/
void ForceSensorModel_valueUpdate(
		struct ForceSensorModel * forceSensorModel) {

	// 1. update data interface value
	ForceSensorInterface_valueUpdate(&forceSensorModel->forceSensorInterface);

	// 2. change interface value to force value
	forceSensorModel->value = forceSensorModel->forceSensorInterface.value * 1.0;
}

