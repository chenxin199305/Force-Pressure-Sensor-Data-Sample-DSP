/*
 * FootPressureForceSensorModel.c
 *
 *  Created on: 2017年10月5日
 *      Author: xin
 */
#include "FootPressureForceSensorModel.h"

/********************************************
 *
 * 	函数说明：足底压力传感器数据模型初始化
 *
 ********************************************/
void FootPressureForceSensorModel_init(
		struct FootPressureForceSensorModel * footPressureForceSensorModel) {

	ForceSensorModel_init(&footPressureForceSensorModel->forceSensorModel_1_1, 1, 1);
	ForceSensorModel_init(&footPressureForceSensorModel->forceSensorModel_1_2, 1, 2);
	ForceSensorModel_init(&footPressureForceSensorModel->forceSensorModel_1_3, 1, 3);
	ForceSensorModel_init(&footPressureForceSensorModel->forceSensorModel_1_4, 1, 4);

	ForceSensorModel_init(&footPressureForceSensorModel->forceSensorModel_2_2, 2, 2);
	ForceSensorModel_init(&footPressureForceSensorModel->forceSensorModel_2_3, 2, 3);
	ForceSensorModel_init(&footPressureForceSensorModel->forceSensorModel_2_4, 2, 4);

	ForceSensorModel_init(&footPressureForceSensorModel->forceSensorModel_3_2, 3, 2);
	ForceSensorModel_init(&footPressureForceSensorModel->forceSensorModel_3_3, 3, 3);

	ForceSensorModel_init(&footPressureForceSensorModel->forceSensorModel_4_2, 4, 2);
	ForceSensorModel_init(&footPressureForceSensorModel->forceSensorModel_4_3, 4, 3);

	ForceSensorModel_init(&footPressureForceSensorModel->forceSensorModel_5_4, 5, 4);

	ForceSensorModel_init(&footPressureForceSensorModel->forceSensorModel_6_1, 6, 1);
	ForceSensorModel_init(&footPressureForceSensorModel->forceSensorModel_6_2, 6, 2);
	ForceSensorModel_init(&footPressureForceSensorModel->forceSensorModel_6_3, 6, 3);
	ForceSensorModel_init(&footPressureForceSensorModel->forceSensorModel_6_4, 6, 4);
}

/********************************************
 *
 * 	函数说明：足底压力传感器数据模型更新
 *
 ********************************************/
void FootPressureForceSensorModel_valueUpdate(
		struct FootPressureForceSensorModel * footPressureForceSensorModel) {

	ForceSensorModel_valueUpdate(&footPressureForceSensorModel->forceSensorModel_1_1);
//	ForceSensorModel_valueUpdate(&footPressureForceSensorModel->forceSensorModel_1_2);
//	ForceSensorModel_valueUpdate(&footPressureForceSensorModel->forceSensorModel_1_3);
//	ForceSensorModel_valueUpdate(&footPressureForceSensorModel->forceSensorModel_1_4);
//
	ForceSensorModel_valueUpdate(&footPressureForceSensorModel->forceSensorModel_2_2);
//	ForceSensorModel_valueUpdate(&footPressureForceSensorModel->forceSensorModel_2_3);
//	ForceSensorModel_valueUpdate(&footPressureForceSensorModel->forceSensorModel_2_4);

//	ForceSensorModel_valueUpdate(&footPressureForceSensorModel->forceSensorModel_3_2);
	ForceSensorModel_valueUpdate(&footPressureForceSensorModel->forceSensorModel_3_3);

//	ForceSensorModel_valueUpdate(&footPressureForceSensorModel->forceSensorModel_4_2);
//	ForceSensorModel_valueUpdate(&footPressureForceSensorModel->forceSensorModel_4_3);
//
//	ForceSensorModel_valueUpdate(&footPressureForceSensorModel->forceSensorModel_5_4);
//
//	ForceSensorModel_valueUpdate(&footPressureForceSensorModel->forceSensorModel_6_1);
//	ForceSensorModel_valueUpdate(&footPressureForceSensorModel->forceSensorModel_6_2);
//	ForceSensorModel_valueUpdate(&footPressureForceSensorModel->forceSensorModel_6_3);
//	ForceSensorModel_valueUpdate(&footPressureForceSensorModel->forceSensorModel_6_4);
}
