//###########################################################################
//
// FILE:   Example_2833xAdcToDMA.c
//
// TITLE:  DSP2833x ADC To DMA
// ASSUMPTIONS:
//
//    This program requires the DSP2833x header files.
//
//    Make sure the CPU clock speed is properly defined in
//    DSP2833x_Examples.h before compiling this example.
//
//    Connect the signals to be converted to channel A0, A1, A2, and A3.
//
//    As supplied, this project is configured for "boot to SARAM"
//    operation.  The 2833x Boot Mode table is shown below.
//    For information on configuring the boot mode of an eZdsp,
//    please refer to the documentation included with the eZdsp,
//
//       $Boot_Table:
//
//         GPIO87   GPIO86     GPIO85   GPIO84
//          XA15     XA14       XA13     XA12
//           PU       PU         PU       PU
//        ==========================================
//            1        1          1        1    Jump to Flash
//            1        1          1        0    SCI-A boot
//            1        1          0        1    SPI-A boot
//            1        1          0        0    I2C-A boot
//            1        0          1        1    eCAN-A boot
//            1        0          1        0    McBSP-A boot
//            1        0          0        1    Jump to XINTF x16
//            1        0          0        0    Jump to XINTF x32
//            0        1          1        1    Jump to OTP
//            0        1          1        0    Parallel GPIO I/O boot
//            0        1          0        1    Parallel XINTF boot
//            0        1          0        0    Jump to SARAM	    <- "boot to SARAM"
//            0        0          1        1    Branch to check boot mode
//            0        0          1        0    Boot to flash, bypass ADC cal
//            0        0          0        1    Boot to SARAM, bypass ADC cal
//            0        0          0        0    Boot to SCI-A, bypass ADC cal
//                                              Boot_Table_End$
//
//
// DESCRIPTION:
//
// ADC is setup to convert 4 channels for each SOC received, with  total of 10 SOCs.
// Each SOC initiates 4 conversions.
// DMA is set up to capture the data on each SEQ1_INT.  DMA will re-sort
// the data by channel sequentially, i.e. all channel0 data will be together
// all channel1 data will be together.
//
// Code should stop in local_DINTCH1_ISR when complete
//
// Watch Variables:
//      DMABuf1
//
//###########################################################################
//
// Original source by: M.P.
//
// $TI Release: DSP2833x Header Files V1.01 $
// $Release Date: September 26, 2007 $
//###########################################################################

#include "DSP2833x_Device.h"     // DSP2833x Headerfile Include File
#include "DSP2833x_Examples.h"   // DSP2833x Examples Include File
#include "HARDWARE/HARDWARE.h"
#include "MODEL_sensors/MODEL_sensors.h"
#include "MODEL_serialPort/MODEL_serialPort.h"
#include "stdio.h"

// ADC start parameters
#if (CPU_FRQ_150MHZ)     // Default - 150 MHz SYSCLKOUT
#define ADC_MODCLK 0x3 // HSPCLK = SYSCLKOUT/2*ADC_MODCLK2 = 150/(2*3)   = 25.0 MHz
#endif
#if (CPU_FRQ_100MHZ)
#define ADC_MODCLK 0x2 // HSPCLK = SYSCLKOUT/2*ADC_MODCLK2 = 100/(2*2)   = 25.0 MHz
#endif
#define ADC_CKPS   0x1   // ADC module clock = HSPCLK/2*ADC_CKPS   = 25.0MHz/(1*2) = 12.5MHz
#define ADC_SHCLK  0xf   // S/H width in ADC module periods                        = 16 ADC clocks
#define ZOFFSET    0x00  // Average Zero offset

// Global variable for this example
volatile Uint16 SampleTable;

extern struct RobotFootPressureForceSensorModel robotFootPressureForceSensorModel;

void main(void)
{
	//---------------------------------------- Basic System Hardware Initialization
	// Step 1. Initialize System Control:
	InitSysCtrl();

	EALLOW;
	SysCtrlRegs.HISPCP.all = ADC_MODCLK;	// HSPCLK = SYSCLKOUT/ADC_MODCLK
	EDIS;

	// Step 2. Initialize GPIO:
	InitGpio();  // Skipped for this example

	// Step 3. Clear all interrupts and initialize PIE vector table:
	DINT;
	InitPieCtrl();	// Initialize the PIE control registers to their default state.
	IER = 0x0000; 	// Disable CPU interrupts and clear all CPU interrupt flags:
	IFR = 0x0000;
	InitPieVectTable();	// Initialize the PIE vector table with pointers to the shell Interrupt

	// Step 4. Initialize all the Device Peripherals:
	InitAdc();  // For this example, init the ADC

	AdcRegs.ADCTRL1.bit.ACQ_PS = ADC_SHCLK;
	AdcRegs.ADCTRL3.bit.ADCCLKPS = ADC_CKPS;
	AdcRegs.ADCTRL1.bit.SEQ_CASC = 1;        // 1  Cascaded mode

	AdcRegs.ADCTRL1.bit.CONT_RUN = 0;		// Setup Start-Stop mode
	AdcRegs.ADCTRL1.bit.SEQ_OVRD = 0;
	AdcRegs.ADCMAXCONV.bit.MAX_CONV1 = 0x0;

	AdcRegs.ADCCHSELSEQ1.bit.CONV00 = 0x0;	// Get Adc from A0
	AdcRegs.ADCCHSELSEQ1.bit.CONV01 = 0x1;
	AdcRegs.ADCCHSELSEQ1.bit.CONV02 = 0x2;
	AdcRegs.ADCCHSELSEQ1.bit.CONV03 = 0x3;
	AdcRegs.ADCCHSELSEQ2.bit.CONV04 = 0x4;
	AdcRegs.ADCCHSELSEQ2.bit.CONV05 = 0x5;
	AdcRegs.ADCCHSELSEQ2.bit.CONV06 = 0x6;
	AdcRegs.ADCCHSELSEQ2.bit.CONV07 = 0x7;
	AdcRegs.ADCCHSELSEQ3.bit.CONV08 = 0x8;
	AdcRegs.ADCCHSELSEQ3.bit.CONV09 = 0x9;
	AdcRegs.ADCCHSELSEQ3.bit.CONV10 = 0xA;
	AdcRegs.ADCCHSELSEQ3.bit.CONV11 = 0xB;
	AdcRegs.ADCCHSELSEQ4.bit.CONV12 = 0xC;
	AdcRegs.ADCCHSELSEQ4.bit.CONV13 = 0xD;
	AdcRegs.ADCCHSELSEQ4.bit.CONV14 = 0xE;
	AdcRegs.ADCCHSELSEQ4.bit.CONV15 = 0xF;

	// Step 5. Initialize Serial Port
	InitSci();
	SerialPortHardware_SciaInit();
	SerialPortHardware_SciaFifoInit();
	SerialPortRegister_clear();

	// Step 6. Initialize Timer
	Timer_InitTimer0(20000);
	Timer_StartTimer0();

	//---------------------------------------- Basic System Hardware Initialization

	MODEL_sensors_init();

	int var = 0;
	// Sensor Test Code (not need to be used in main process)
	while(0) {

		for (var = 0; var < 1000; ++var) {
		}

		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL2_CLEAR;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL1_CLEAR;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL0_CLEAR;

		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL2_CLEAR;
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL1_CLEAR;
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL0_SET;

		for (var = 0; var < 1000; ++var) {
		}

		// Start SEQ1
		AdcRegs.ADCTRL2.bit.SOC_SEQ1 = 1;			// Start Convert of SEQ1
		while (AdcRegs.ADCST.bit.INT_SEQ1 == 0); 	// Wait for interrupt
		AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1;       	// Clear INT SEQ1 bit
		AdcRegs.ADCTRL2.bit.RST_SEQ1 = 1;			// Reset SEQ1
		SampleTable = AdcRegs.ADCRESULT0 >> 4;
		printf("SampleTable32 = %d\n", SampleTable);

		for (var = 0; var < 1000; ++var) {
		}

		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL2_CLEAR;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL1_CLEAR;
		FORCE_SENSOR_INTERFACE_INPUT_MUX_SEL0_CLEAR;

		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL2_CLEAR;
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL1_SET;
		FORCE_SENSOR_INTERFACE_OUTPUT_MUX_SEL0_CLEAR;

		for (var = 0; var < 500; ++var) {
		}

		// Start SEQ1
		AdcRegs.ADCTRL2.bit.SOC_SEQ1 = 1;			// Start Convert of SEQ1
		while (AdcRegs.ADCST.bit.INT_SEQ1 == 0); 	// Wait for interrupt
		AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1;       	// Clear INT SEQ1 bit
		AdcRegs.ADCTRL2.bit.RST_SEQ1 = 1;			// Reset SEQ1
		SampleTable = AdcRegs.ADCRESULT0 >> 4;
		printf("SampleTable33 = %d\n", SampleTable);

	}

	while(0) {
		RobotFootPressureForceSensorModel_valueUpdate(&robotFootPressureForceSensorModel);
		printf("leftLegFootPressureForceSensorModel.forceSensorModel_1_1.value = %f \n", robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_1_1.value);
		//		printf("leftLegFootPressureForceSensorModel.forceSensorModel_1_2.value = %f \n", robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_1_2.value);
		//		printf("leftLegFootPressureForceSensorModel.forceSensorModel_1_3.value = %f \n", robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_1_3.value);
		//		printf("leftLegFootPressureForceSensorModel.forceSensorModel_1_4.value = %f \n", robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_1_4.value);
	}

	EINT;
	for(;;) {};

	printf("out of the program.\n");
}


