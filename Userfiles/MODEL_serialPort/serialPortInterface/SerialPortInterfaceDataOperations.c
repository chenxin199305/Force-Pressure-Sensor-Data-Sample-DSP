/*
 * SerialPortInterfaceDataOperations.c
 *
 *  Created on: 2017年3月15日
 *      Author: xin
 */

#include "SerialPortInterfaceDataOperations.h"

/**
 *	函数说明：	从串口接收到的数据中组合出 float 类型的数据
 */
float SerialPortInterfaceDataOperations_getFloatValueFrom(unsigned int * lowerVal, unsigned int * higherVal) {

	float result;
	unsigned long temp = ((unsigned long)(*lowerVal) + ((unsigned long)(*higherVal) << 16));

	result = *(float*)(&temp);

	return result;
}

/**
 *	函数说明：	从串口接收到的数据中得出 int 类型的数据
 */
int SerialPortInterfaceDataOperations_getInt16ValFrom(unsigned int * val) {

	int result;

	result = *(int*)val;

	return result;
}

/**
 *	函数说明：	从串口接收到的数据中组合出 long 类型的数据
 */
long SerialPortInterfaceDataOperations_getInt32ValFrom(unsigned int * lowerVal, unsigned int * higherVal) {

	long result;

	result = (long)((unsigned long)(*lowerVal) + ((unsigned long)(*higherVal) << 16));

	return result;
}

/**
 *	函数说明：	从串口接收到的数据中组合出 unsigned long 类型的数据
 */
unsigned long SerialPortInterfaceDataOperations_getUint32ValFrom(unsigned int * lowerVal, unsigned int * higherVal) {

	unsigned long result;

	result = ((unsigned long)(*lowerVal) + ((unsigned long)(*higherVal) << 16));

	return result;
}

/**
 *	函数说明：	将 float 格式数据变换成 unsigned long 格式
 */
unsigned long SerialPortInterfaceDataOperations_getUint32ValFromFloat(float * val) {

	unsigned long result;

	result = *(unsigned long *)val;

	return result;
}


