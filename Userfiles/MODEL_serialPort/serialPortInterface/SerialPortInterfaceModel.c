/*
 * SerialPortInterfaceModel.c
 *
 *  Created on: 2017年3月9日
 *      Author: xin
 */

#include "SerialPortInterfaceModel.h"

int SciaXmtBufIndex = 0;
int SciaXmtBufNum = 0;										// 串口发送的数据量计数
unsigned int SciaXmtBuf[SCIA_XMT_BUF_SIZE];					// 串口发送数据缓存数组

int SciaRcvBufIndex = 0;
int SciaRcvBufNum = 0;										// 串口接收的数据量计数
unsigned int SciaRcvBuf[SCIA_RCV_BUF_SIZE];					// 串口接收数据缓存数组

#pragma DATA_SECTION(SerialPortRegister, ".serialPortRegister");
unsigned int SerialPortRegister[SERIAL_PORT_REGISTER_BUFFER_SIZE];

/************************************
 *
 *	函数说明：	清空接收缓存 SciaRcvBuf
 *
 ************************************/
static void SciaRcvBuf_clear(void) {

	int i;
	for (i = 0; i < SCIA_RCV_BUF_SIZE; i++) {
		SciaRcvBuf[i] = 0;
	}
	SciaRcvBufIndex = 0;
	SciaRcvBufNum = 0;
}

/************************************
 *
 *	函数说明：	清空发送缓存 SciaXmtBuf
 *
 ************************************/
static void SciaXmtBuf_clear(void) {

	int i;
	for (i = 0; i < SCIA_XMT_BUF_SIZE; i++) {
		SciaXmtBuf[i] = 0;
	}
	SciaXmtBufIndex = 0;
	SciaXmtBufNum = 0;
}

/************************************
 *
 *	函数说明：	读取 SerialPortRegister 的值
 *
 ************************************/
unsigned int SerialPortRegister_get(unsigned int address) {

	if (address >= SERIAL_PORT_REGISTER_BUFFER_SIZE) {
		return 0;
	}
	else {
		return SerialPortRegister[address];
	}
}

/************************************
 *
 *	函数说明：	设置 SerialPortRegister 的值
 *
 ************************************/
void SerialPortRegister_set(unsigned int address, unsigned int value) {

	if (address >= SERIAL_PORT_REGISTER_BUFFER_SIZE) {
		return;
	}
	else {
		SerialPortRegister[address] = value;
	}
}


/************************************
 *
 *	函数说明：		清空缓存数据
 *
 ************************************/
void SerialPortRegister_clear(void) {
	int i;
	for (i = 0; i < SERIAL_PORT_REGISTER_BUFFER_SIZE; i++) {
		SerialPortRegister[i] = 0;
	}
}

/************************************
 *
 *	函数说明：		启动发送
 *
 ************************************/
static void SciaXmtStart(void) {
	SciaRegs.SCIFFTX.bit.TXFFIENA = 1;
}

/************************************
 *
 *	函数说明：		发送一个字符
 *
 ************************************/
void scia_xmit(unsigned int a)
{
	SciaXmtBuf[SciaXmtBufIndex] = a;
	SciaXmtBufNum++;
	SciaXmtStart();
}

/************************************
 *
 *	函数说明：发送一串字符串
 *
 ************************************/
void SciaXmt(unsigned int *buf, int length)
{
	// 0. 清除上次发送的内容
	int i;
	for (i = 0; i < SCIA_XMT_BUF_SIZE; ++i) {
		SciaXmtBuf[i] = 0;
	}

	// 1. 填入数据
	if (length >= SCIA_XMT_BUF_SIZE) {
		length = SCIA_XMT_BUF_SIZE;
	}

	for (i = 0; i < length; ++i) {
		SciaXmtBuf[i] = *(buf + i);
	}

	SciaXmtBufIndex	= 0;
	SciaXmtBufNum 	= SCIA_XMT_BUF_SIZE;

	SciaXmtStart();
}

/************************************
 *
 *	函数说明：		发送256个字节的数据
 *
 ************************************/
void scia_msg(char * msg)
{
	int i = 0;
	unsigned int index = SciaXmtBufIndex;

	while(msg[i] != '\0')
	{
		if(index == SCIA_XMT_BUF_SIZE) {
			index = 0;
		}

		SciaXmtBuf[index++] = msg[i++];
		SciaXmtBufNum++;

		if(i > SCIA_XMT_BUF_SIZE) {
			break;
		}
	}
	SciaXmtStart();
}

/*************************************
 *
 *	函数说明：	在串口接收到的缓存中查找数据帧
 *
 *	@edit by xin:	原先的程序读取数据的时候总容易漏帧，尝试进行修改
 *
 ************************************/
int FindFrameHeader_A(void)
{
	int i, j;

	// 1. 至少应该存在 标志头(2) + 数据长度(1) + 结尾校验(1)
	if(SciaRcvBufNum < 4) {
		return -1;
	}
	else {
		for(i = 0; i < (SciaRcvBufNum - 3); i++) {

			// 2. 寻找头标志
			if((SciaRcvBuf[i + 0] == 0xFF) && (SciaRcvBuf[i + 1] == 0xFA)) {

				// 3. 获取数据段长度
				int dataLength = SciaRcvBuf[i + 2];

				// 4. 检查数据长度
				if ((SciaRcvBufNum - (i + 3)) >= dataLength) {

					// 5. 比较校验和
					unsigned int checksum = 0;

					for(j = 0; j < (dataLength - 1); j++) {
						checksum += SciaRcvBuf[i + 3 + j];
					}
					checksum = checksum & 0xFF;

					if (checksum == SciaRcvBuf[i + 2 + dataLength]) {
					}
					else {
						// 校验和不对，跳过该组数据
						checksum = 0;
						continue;
					}

					// 6. 获取有效数据
					for(j = 0; j < (dataLength - 1); j = j + 4) {
						unsigned int addr = (SciaRcvBuf[i + 3 + j] << 8) 		| SciaRcvBuf[i + 3 + j + 1];
						unsigned int data = (SciaRcvBuf[i + 3 + j + 2] << 8) 	| SciaRcvBuf[i + 3 + j + 3];
						if(addr >= SERIAL_PORT_REGISTER_BUFFER_SIZE)
							return -2;
						else
							SerialPortRegister[addr] = data;
					}

					// 7. 删除前面所有数据(后面的数据往前面移动)
					SciaRcvBuf_clear();
					//					for (j = (i + 3 + dataLength); j < SCIA_RCV_BUF_SIZE; j++) {
					//						SciaRcvBuf[j - (i + 3 + dataLength)] = SciaRcvBuf[j];
					//					}
					//					for (j = 0; j < (i + 3 + dataLength); j++) {
					//						SciaRcvBuf[SCIA_RCV_BUF_SIZE - (i + 3 + dataLength) + j] = 0;
					//					}
					//					SciaRcvBufNum = SciaRcvBufNum - (i + 3 + dataLength);

					return 0;
				}
				else {
					return -4;
				}
			}
		}

		return -5;
	}
}

/*****************************
 *
 *	函数说明：	接收中断函数
 *
 *****************************/
interrupt void ISRSciaRx(void) {
	if(SciaRegs.SCIFFRX.bit.RXFFOVF == 1) {
		SciaRegs.SCIFFRX.bit.RXFFOVRCLR = 1;
		SciaRcvBuf_clear();
	}
	else if(SciaRegs.SCIFFRX.bit.RXFFINT == 1) {
		/**
		 *	说明：	num 串口接收到的字节数目
		 */
		int i;
		unsigned int num = SciaRegs.SCIFFRX.bit.RXFFST;
		for(i = 0; i < num; i++) {
			if(SciaRcvBufNum >= SCIA_RCV_BUF_SIZE) {
				SciaRcvBuf_clear();
			}
			else {
				SciaRcvBuf[SciaRcvBufNum] = SciaRegs.SCIRXBUF.bit.RXDT;
				SciaRcvBufNum++;
			}
		}
		FindFrameHeader_A();
		SciaRegs.SCIFFRX.bit.RXFFINTCLR = 1;
	}

	// Acknowledge this interrupt to receive more interrupts from group 9
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP9;
}

/******************************
 *
 *	函数说明：	发送中断函数
 *
 *	注意：		由于发送数据比较多，这里需要考虑耗时问题！！！
 *
 *****************************/
interrupt void ISRSciaTx(void) {
	if(SciaRegs.SCIFFTX.bit.TXFFINT == 1) {
		int i;
		unsigned int num = 16 - SciaRegs.SCIFFTX.bit.TXFFST;
		if(num > (SciaXmtBufNum - SciaXmtBufIndex)) {
			num = (SciaXmtBufNum - SciaXmtBufIndex);
		}

		for(i = 0; i < num; i++) {
			SciaRegs.SCITXBUF = SciaXmtBuf[SciaXmtBufIndex + i];
		}

		SciaXmtBufIndex += num;
		if(SciaXmtBufIndex >= SciaXmtBufNum) {
			SciaRegs.SCIFFTX.bit.TXFFIENA = 0;
		}
		SciaRegs.SCIFFTX.bit.TXFFINTCLR = 1;
	}

	// Acknowledge this interrupt to receive more interrupts from group 9
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP9;
}


