/*
 * SerialPortInterafceModel.h
 *
 *  Created on: 2017年3月9日
 *      Author: xin
 */

#ifndef USERFILES_MODEL_SERIALPORTFRAME_SERIALPORTINTERFACE_SERIALPORTINTERFACEMODEL_H_
#define USERFILES_MODEL_SERIALPORTFRAME_SERIALPORTINTERFACE_SERIALPORTINTERFACEMODEL_H_

#include "DSP2833x_Device.h"    // DSP2833x Headerfile Include File
#include "DSP2833x_Examples.h" 	// DSP2833x Examples Include File

//----------------------------------------------macro definition
#define SERIAL_PORT_REGISTER_BUFFER_SIZE		2048

#define SCIA_XMT_BUF_SIZE 		64
#define SCIA_RCV_BUF_SIZE 		128
//----------------------------------------------macro definition

//-----------------------------------------------functions
unsigned int SerialPortRegister_get(unsigned int address);
void SerialPortRegister_set(unsigned int address, unsigned int value);
void SerialPortRegister_clear(void);

void scia_xmit(unsigned int a);
void SciaXmt(unsigned int *buf, int length);
void scia_msg(char *msg);

interrupt void ISRSciaRx(void);
interrupt void ISRSciaTx(void);
//-----------------------------------------------functions

#endif /* USERFILES_MODEL_SERIALPORTFRAME_SERIALPORTINTERFACE_SERIALPORTINTERFACEMODEL_H_ */
