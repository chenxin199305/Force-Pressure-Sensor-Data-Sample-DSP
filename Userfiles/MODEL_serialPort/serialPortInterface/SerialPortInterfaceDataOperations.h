/*
 * SerialPortInterfaceDataOperations.h
 *
 *  Created on: 2017年3月15日
 *      Author: xin
 */

#ifndef USERFILES_MODEL_SERIALPORTFRAME_SERIALPORTINTERFACE_SERIALPORTINTERFACEDATAOPERATIONS_H_
#define USERFILES_MODEL_SERIALPORTFRAME_SERIALPORTINTERFACE_SERIALPORTINTERFACEDATAOPERATIONS_H_


float 			SerialPortInterfaceDataOperations_getFloatValueFrom(unsigned int * lowerVal, unsigned int * higherVal);
int 			SerialPortInterfaceDataOperations_getInt16ValFrom(unsigned int * val);
long 			SerialPortInterfaceDataOperations_getInt32ValFrom(unsigned int * lowerVal, unsigned int * higherVal);
unsigned long 	SerialPortInterfaceDataOperations_getUint32ValFrom(unsigned int * lowerVal, unsigned int * higherVal);
unsigned long 	SerialPortInterfaceDataOperations_getUint32ValFromFloat(float * val);



#endif /* USERFILES_MODEL_SERIALPORTFRAME_SERIALPORTINTERFACE_SERIALPORTINTERFACEDATAOPERATIONS_H_ */
