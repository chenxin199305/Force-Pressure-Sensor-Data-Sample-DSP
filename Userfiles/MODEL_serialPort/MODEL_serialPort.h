/*
 * MODEL_serialPort.h
 *
 *  Created on: 2017年3月9日
 *      Author: xin
 */

#ifndef USERFILES_MODEL_SERIALPORT_MODEL_SERIALPORT_H_
#define USERFILES_MODEL_SERIALPORT_MODEL_SERIALPORT_H_

#include "serialPortInterface/SerialPortInterfaceDataOperations.h"
#include "serialPortInterface/SerialPortInterfaceModel.h"
#include "serialPortSendFrame/SerialPortSendFrameModel.h"

#endif /* USERFILES_MODEL_SERIALPORT_MODEL_SERIALPORT_H_ */
