/*
 * SerialPortSendFrameModel.h
 *
 *  Created on: 2017年3月9日
 *      Author: xin
 */

#ifndef USERFILES_MODEL_SERIALPORTFRAME_SERIALPORTSENDFRAME_SERIALPORTSENDFRAMEMODEL_H_
#define USERFILES_MODEL_SERIALPORTFRAME_SERIALPORTSENDFRAME_SERIALPORTSENDFRAMEMODEL_H_

#define SERIAL_PORT_SEND_FRAME_MODEL_HEADER1		0xFF
#define SERIAL_PORT_SEND_FRAME_MODEL_HEADER2		0xF0

struct SerialPortSendFrameModel {

	unsigned int head1;
	unsigned int head2;
	unsigned int size;
	unsigned int * buff;
	unsigned int checkSum;
};

void SerialPortSendFrameModel_init_hhsb(
		struct SerialPortSendFrameModel * serialPortSendFrameModel,
		unsigned int head1,
		unsigned int head2,
		unsigned int size,
		unsigned int * buff);

void SerialPortSendFrameModel_init_sb(
		struct SerialPortSendFrameModel * serialPortSendFrameModel,
		unsigned int size,
		unsigned int * buff);

void SerialPortSendFrameModel_send(
		struct SerialPortSendFrameModel * serialPortSendFrameModel);




#endif /* USERFILES_MODEL_SERIALPORTFRAME_SERIALPORTSENDFRAME_SERIALPORTSENDFRAMEMODEL_H_ */
