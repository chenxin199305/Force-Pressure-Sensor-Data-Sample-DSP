/*
 * SerialPortSendFrameModel.c
 *
 *  Created on: 2017年3月9日
 *      Author: xin
 */

#include "stdlib.h"
#include "SerialPortSendFrameModel.h"
#include "../serialPortInterface/SerialPortInterfaceModel.h"

/*****************************
 *
 *	函数说明：	初始化串口发送模型
 *
 ****************************/
void SerialPortSendFrameModel_init_hhsb(
		struct SerialPortSendFrameModel * serialPortSendFrameModel,
		unsigned int head1,
		unsigned int head2,
		unsigned int size,
		unsigned int * buff) {

	serialPortSendFrameModel->head1 = head1;
	serialPortSendFrameModel->head2 = head2;
	serialPortSendFrameModel->size = size;
	serialPortSendFrameModel->buff = buff;

	int i;
	for (i = 0; i < size; i++) {
		serialPortSendFrameModel->checkSum += *(serialPortSendFrameModel->buff + i);
	}
	serialPortSendFrameModel->checkSum = serialPortSendFrameModel->checkSum & 0xFF;
}

/*****************************
 *
 *	函数说明：	初始化串口发送模型
 *
 ****************************/
void SerialPortSendFrameModel_init_sb(
		struct SerialPortSendFrameModel * serialPortSendFrameModel,
		unsigned int size,
		unsigned int * buff) {

	serialPortSendFrameModel->head1 = SERIAL_PORT_SEND_FRAME_MODEL_HEADER1;
	serialPortSendFrameModel->head2 = SERIAL_PORT_SEND_FRAME_MODEL_HEADER2;
	serialPortSendFrameModel->size = size;
	serialPortSendFrameModel->buff = buff;

	int i;
	for (i = 0; i < size; i++) {
		serialPortSendFrameModel->checkSum += *(serialPortSendFrameModel->buff + i);
	}
	serialPortSendFrameModel->checkSum = serialPortSendFrameModel->checkSum & 0xFF;
}

/*****************************
 *
 *	函数说明：	更新数值
 *
 ****************************/
void SerialPortSendFrameModel_updateBuff() {

}

/*****************************
 *
 *	函数说明：	串口发送模型 数据发送
 *
 ****************************/
void SerialPortSendFrameModel_send(
		struct SerialPortSendFrameModel * serialPortSendFrameModel) {

	unsigned int tempBuffSize = 1 + 1 + 1 + serialPortSendFrameModel->size + 1;

	unsigned int * tempBuff = (unsigned int *)malloc(tempBuffSize);

	tempBuff[0] = serialPortSendFrameModel->head1;
	tempBuff[1] = serialPortSendFrameModel->head2;
	tempBuff[2] = serialPortSendFrameModel->size;

	int i;
	for (i = 0; i < serialPortSendFrameModel->size; i++) {
		tempBuff[3 + i] = serialPortSendFrameModel->buff[i];
	}

	tempBuff[tempBuffSize - 1] = 0;
	for (i = 0; i < serialPortSendFrameModel->size; ++i) {
		tempBuff[tempBuffSize - 1] = (unsigned int)(tempBuff[tempBuffSize - 1] + tempBuff[3 + i]);
	}
	tempBuff[tempBuffSize - 1] = tempBuff[tempBuffSize - 1] & 0xFF;

	SciaXmt(tempBuff, tempBuffSize);

	free(tempBuff);
}

