//###########################################################################
//
// FILE:   		Tick.c
//
// TITLE:  		DSP28335 Timer Interrupt Service Routine.
//
//###########################################################################

#include "DSP2833x_Device.h"     // DSP2833x Headerfile Include File
#include "DSP2833x_Examples.h"   // DSP2833x Examples Include File
#include "HARDWARE/HARDWARE.h"
#include "MODEL_sensors/MODEL_sensors.h"
#include "MODEL_serialPort/MODEL_serialPort.h"
#include "stdio.h"

/*****************************
 *
 *	变量说明：
 *
 *****************************/
extern struct RobotFootPressureForceSensorModel robotFootPressureForceSensorModel;

/*****************************
 *
 *	函数说明：	中断函数
 *
 *****************************/
interrupt void Tmier0Tick(void)
{
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;

	//----------------------------------------Update Sensor Data

	RobotFootPressureForceSensorModel_valueUpdate(&robotFootPressureForceSensorModel);
//	printf("leftLegFootPressureForceSensorModel.forceSensorModel_6_1.value = %f \n", robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_6_1.value);
//	printf("leftLegFootPressureForceSensorModel.forceSensorModel_6_2.value = %f \n", robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_6_2.value);
//	printf("leftLegFootPressureForceSensorModel.forceSensorModel_6_3.value = %f \n", robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_6_3.value);
//	printf("leftLegFootPressureForceSensorModel.forceSensorModel_6_4.value = %f \n", robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_6_4.value);
	printf("leftLegFootPressureForceSensorModel.forceSensorModel_3_2.value = %f \n", robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_3_2.value);
	printf("leftLegFootPressureForceSensorModel.forceSensorModel_3_3.value = %f \n", robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_3_3.value);

	//----------------------------------------Update Sensor Data

	//----------------------------------------Send Sensor Data

	unsigned int buffer[32];

	buffer[0] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_1_1.forceSensorInterface.value >> 8) & 0xFF;
	buffer[1] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_1_1.forceSensorInterface.value >> 0) & 0xFF;
	buffer[2] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_1_2.forceSensorInterface.value >> 8) & 0xFF;
	buffer[3] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_1_2.forceSensorInterface.value >> 0) & 0xFF;
	buffer[4] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_1_3.forceSensorInterface.value >> 8) & 0xFF;
	buffer[5] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_1_3.forceSensorInterface.value >> 0) & 0xFF;
	buffer[6] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_1_4.forceSensorInterface.value >> 8) & 0xFF;
	buffer[7] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_1_4.forceSensorInterface.value >> 0) & 0xFF;
	buffer[8] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_2_2.forceSensorInterface.value >> 8) & 0xFF;
	buffer[9] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_2_2.forceSensorInterface.value >> 0) & 0xFF;
	buffer[10] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_2_3.forceSensorInterface.value >> 8) & 0xFF;
	buffer[11] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_2_3.forceSensorInterface.value >> 0) & 0xFF;
	buffer[12] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_2_4.forceSensorInterface.value >> 8) & 0xFF;
	buffer[13] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_2_4.forceSensorInterface.value >> 0) & 0xFF;
	buffer[14] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_3_2.forceSensorInterface.value >> 8) & 0xFF;
	buffer[15] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_3_2.forceSensorInterface.value >> 0) & 0xFF;
	buffer[16] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_3_3.forceSensorInterface.value >> 8) & 0xFF;
	buffer[17] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_3_3.forceSensorInterface.value >> 0) & 0xFF;
	buffer[18] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_4_2.forceSensorInterface.value >> 8) & 0xFF;
	buffer[19] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_4_2.forceSensorInterface.value >> 0) & 0xFF;
	buffer[20] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_4_3.forceSensorInterface.value >> 8) & 0xFF;
	buffer[21] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_4_3.forceSensorInterface.value >> 0) & 0xFF;
	buffer[22] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_5_4.forceSensorInterface.value >> 8) & 0xFF;
	buffer[23] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_5_4.forceSensorInterface.value >> 0) & 0xFF;
	buffer[24] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_6_1.forceSensorInterface.value >> 8) & 0xFF;
	buffer[25] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_6_1.forceSensorInterface.value >> 0) & 0xFF;
	buffer[26] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_6_2.forceSensorInterface.value >> 8) & 0xFF;
	buffer[27] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_6_2.forceSensorInterface.value >> 0) & 0xFF;
	buffer[28] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_6_3.forceSensorInterface.value >> 8) & 0xFF;
	buffer[29] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_6_3.forceSensorInterface.value >> 0) & 0xFF;
	buffer[30] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_6_4.forceSensorInterface.value >> 8) & 0xFF;
	buffer[31] = (robotFootPressureForceSensorModel.leftLegFootPressureForceSensorModel.forceSensorModel_6_4.forceSensorInterface.value >> 0) & 0xFF;

	struct SerialPortSendFrameModel serialPortSendFrameModel;

	SerialPortSendFrameModel_init_sb(&serialPortSendFrameModel, 32, buffer);

	SerialPortSendFrameModel_send(&serialPortSendFrameModel);

	//----------------------------------------Send Sensor Data

}

