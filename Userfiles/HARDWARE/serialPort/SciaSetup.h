/*
 * ScibSetup.h
 *
 *  Created on: 2013-11-18
 *      Author: xin
 */

#ifndef SCIASETUP_H_
#define SCIASETUP_H_

void SerialPortHardware_SciaInit(void);
void SerialPortHardware_SciaFifoInit(void);

//===========================================================================
// No more.
//===========================================================================

#endif /* SCIBSETUP_H_ */
