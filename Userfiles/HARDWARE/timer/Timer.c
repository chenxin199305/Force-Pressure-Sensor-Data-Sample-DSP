//###########################################################################
//
// FILE:   		Timer.c
//
// TITLE:  		DSP28335 Timer Initialization & Support Functions.
//
//###########################################################################
// EDIT BY: 	RenHuichao
// DATE:		2014.12.18
//###########################################################################

#include "Timer.h"				// CPU Timer header file

extern interrupt void Tmier0Tick(void);

//---------------------------------------------------------------------------
// FUNCTION:	void InitTimer(float uSecond):
//---------------------------------------------------------------------------
// DESCRIBE:	This function sets the CPU Timer period
//---------------------------------------------------------------------------
// PARAMETERS:
// 				float32 uSecond:		CPU timer period in US
//										Up to 28 seconds

/*
 *	函数说明：	定时器0初始化
 */
void Timer_InitTimer0(float uSecond)
{
    // Initialize timer0 period:
    CpuTimer0Regs.PRD.all = (long) (150.0 * uSecond);

    // Set pre-scale counter to divide by 1 (SYSCLKOUT):
    CpuTimer0Regs.TPR.all  = 0;
    CpuTimer0Regs.TPRH.all  = 0;

    // Initialize timer control register:
    CpuTimer0Regs.TCR.bit.TSS = 1;      // 1 = Stop timer, 0 = Start/Restart Timer
    CpuTimer0Regs.TCR.bit.TRB = 1;      // 1 = reload timer
    CpuTimer0Regs.TCR.bit.SOFT = 1;
    CpuTimer0Regs.TCR.bit.FREE = 1;     // Timer Free Run
    CpuTimer0Regs.TCR.bit.TIE = 1;      // 0 = Disable/ 1 = Enable Timer Interrupt

    CpuTimer0.InterruptCount = 0;

    // Enable INT1 interrupt
	IER |= M_INT1;

	// Enable TINT0 interrupt in group 1
	PieCtrlRegs.PIEIER1.bit.INTx7 = 1;

	//
	EALLOW;
	PieVectTable.TINT0 = &Tmier0Tick;
	EDIS;
}

//---------------------------------------------------------------------------
// FUNCTION:	void StartTimer0(void)
//---------------------------------------------------------------------------
// DESCRIBE:	This function start the CPU Timer0 tick

void Timer_StartTimer0(void)
{
	CpuTimer0Regs.TCR.bit.TRB = 1;      // 1 = reload timer
	CpuTimer0Regs.TCR.bit.TSS = 0;
}


//---------------------------------------------------------------------------
// FUNCTION:	void StopTimer0(void)
//---------------------------------------------------------------------------
// DESCRIBE:	This function stop the CPU Timer0 tick

void Timer_StopTimer0(void)
{
	CpuTimer0Regs.TCR.bit.TSS = 1;
}

