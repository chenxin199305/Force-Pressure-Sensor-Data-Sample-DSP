//###########################################################################
//
// FILE:   		Timer.h
//
// TITLE:  		DSP28335 Timer Initialization & Support Functions header files
//
//###########################################################################
// EDIT BY: 	RenHuichao
// DATE:		2014.12.18
//###########################################################################

#ifndef F28335_TIMER_H_
#define F28335_TIMER_H_

#include "DSP2833x_Device.h"    // DSP2833x Headerfile Include File
#include "DSP2833x_Examples.h" 	// DSP2833x Examples Include File
#include "DSP2833x_CpuTimers.h"

void Timer_InitTimer0(float uSecond);
void Timer_StartTimer0(void);
void Timer_StopTimer0(void);

#endif /* TIMER_H_ */
